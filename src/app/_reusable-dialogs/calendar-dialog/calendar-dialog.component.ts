import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog'
import { map, catchError, first, retry } from 'rxjs/operators'
import { Observable, throwError } from 'rxjs'
import { FormGroup, FormBuilder, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';



@Component({
  selector: 'app-calendar-dialog',
  templateUrl: './calendar-dialog.component.html',
  styleUrls: ['./calendar-dialog.component.scss']
})
export class CalendarDialogComponent implements OnInit {
  eventForm: FormGroup;
  isConfirmed: boolean = false;
  eventData: any;
  dialogData:any;
  constructor(
    public dialogRef: MatDialogRef<CalendarDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data,
    private formBuilder: FormBuilder
  ) {

    console.log(data);
    this.eventData = data;
  }

  ngOnInit() {
    this.dialogData = {
      isConfirmed: this.isConfirmed,
      eventData: this.eventData
    }
    this.createForm();
    this.dialogRef.beforeClosed().subscribe(() => this.dialogRef.close(this.dialogData))
  }
  createForm() {
    this.eventForm = this.formBuilder.group({
      title: [''],
      color: ['']
    })
  }
  onSubmit(){
    this.dialogData.isConfirmed = true;
    this.dialogData.eventFormdata = this.eventForm.getRawValue();
    this.dialogRef.close(this.dialogData)
  }
}
