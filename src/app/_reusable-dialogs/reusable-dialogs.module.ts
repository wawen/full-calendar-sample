import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialsModule } from 'src/app/shared/materials.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CalendarDialogComponent } from './calendar-dialog/calendar-dialog.component';



@NgModule({
  declarations: [
    CalendarDialogComponent
  ],
  exports: [
    CalendarDialogComponent
  ],
  imports: [
    CommonModule,
    MaterialsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    CalendarDialogComponent
  ],
  providers: [

  ],
  schemas:[

  ]
})
export class ReusableDialogsModule { }
