import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FullCalendarComponent, CalendarOptions, DateSelectArg, EventClickArg, EventApi } from '@fullcalendar/angular';
import { INITIAL_EVENTS, createEventId } from './event-utils';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';
import resourceTimeGridPlugin from '@fullcalendar/resource-timegrid';
import dayGridPlugin from '@fullcalendar/daygrid';
import { CalendarDialogComponent } from 'src/app/_reusable-dialogs/calendar-dialog/calendar-dialog.component';
import { MatDialog } from '@angular/material/dialog'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  // references the #calendar in the template
  @ViewChild('fullcalendar') calendarComponent: FullCalendarComponent;
  @ViewChild('externalDraggableItems', {static: true}) externalDraggableItems: ElementRef;

  // testcommit test again Test Change Alias

  calendarVisible = true;
  calendarOptions: CalendarOptions;
  currentEvents: EventApi[] = [];
  filterCurrentEvents: EventApi[] = [];
  constructor(public dialog: MatDialog){

  }
  ngOnInit() {
    this.calendarOptions = {
      plugins: [dayGridPlugin, interactionPlugin, timeGridPlugin, resourceTimeGridPlugin],
      headerToolbar: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
      },
      initialView: 'dayGridMonth',
      // initialEvents: INITIAL_EVENTS, // alternatively, use the `events` setting to fetch from a feed
      weekends: true,
      editable: true,
      selectable: true,
      droppable: true,
      selectMirror: true,
      dayMaxEvents: true,
      select: this.handleDateSelect.bind(this),
      eventClick: this.handleEventClick.bind(this),
      eventsSet: this.handleEvents.bind(this),
      eventDrop: this.handleEventDropInternal.bind(this),
      eventReceive: this.handleEventDropExternal.bind(this),
      // drop: function() {
      //   alert('dropped!');
      // },
      eventAdd: (event) => ( console.log(event) )
      /* you can update a remote database when these fire:
      eventAdd:
      eventChange:
      eventRemove:
      */
    };
    document.addEventListener('DOMContentLoaded', () => {
      console.log("GUMAGANA!")
      // let draggableEl = document.getElementById('draggable-el');
      // console.log(draggableEl);
      // new Draggable(draggableEl);
      new Draggable(this.externalDraggableItems.nativeElement, {
          itemSelector: '.draggable-el',
          eventData: (eventEl) => {
            console.log(eventEl);
            return {
              title: eventEl.getAttribute("data-title"),
              start: eventEl.getAttribute("data-start"),
              end: eventEl.getAttribute("data-end"),
              duration: eventEl.getAttribute("data-sampleDuration"),
              color: "blue",
              // backgroundColor: "red",
            };
          }
      });
      new Draggable(this.externalDraggableItems.nativeElement, {
          itemSelector: '.draggable-el-duration',
          eventData: (eventEl) => {
            console.log(eventEl);
            const time =  new Date();
            console.log(time)
            console.log(this.currentEvents)
            return {
              title: eventEl.getAttribute("data-title"),
              // start: time,
              // end: eventEl.getAttribute("data-end"),
              startTime: '10:45:00',
              endTime: '12:45:00',
              duration: '02:00',
              color: "red",
              allDay: false
              // backgroundColor: "red",
            };
          }
      });
    });

  }

  handleCalendarToggle() {
    this.calendarVisible = !this.calendarVisible;
  }

  handleWeekendsToggle() {
    const { calendarOptions } = this;
    calendarOptions.weekends = !calendarOptions.weekends;
  }

  handleDateSelect(selectInfo: DateSelectArg) {
    //PROMP CAN BE CHANGE INTO OWN MODAL
    console.log(selectInfo)
    const title = prompt('Please enter a new title for your event');
    const calendarApi = selectInfo.view.calendar;

    calendarApi.unselect(); // clear date selection
    if (title) {
      calendarApi.addEvent({
        id: createEventId(),
        title,
        // start: selectInfo.startStr,
        // end: selectInfo.endStr,
        // start: selectInfo.start,
        // end: selectInfo.end,
        // daysOfWeek: [ '4' ],
        // startTime: selectInfo.startStr + '11:00:00',
        // endTime: selectInfo.endStr + '13:30:00',
        // startTime: selectInfo.startStr + '11:00:00',
        // endTime: selectInfo.endStr + '13:30:00',
        startRecur: selectInfo.startStr,
        endRecur: selectInfo.endStr,
        startTime: "04:00",
        endTime: "14:00",
        // duration: "02:30",
        recurring: false,
        allDay: false
      });
    }
  }

  handleEventClick(clickInfo: EventClickArg) {
    console.log(clickInfo)
    console.log(clickInfo.event.title)
    const dialogRef = this.dialog.open(CalendarDialogComponent, {
      disableClose: false,
      data: {
        option: 'eventClick',
        calendar: clickInfo.event
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      console.log(res)
      if (res.isConfirmed) {
        clickInfo.event.setProp('title', res.eventFormdata.title)
        clickInfo.event.setProp('color', res.eventFormdata.color)
      }

    })

    // if (confirm(`Are you sure you want to delete the event '${clickInfo.event.title}'`)) {
    //   clickInfo.event.remove();
    //   // clickInfo.event.editEvent()
    // }
  }

  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
    this.filterCurrentEvents = events;
  }

  handleEventDropExternal(events) {
    // const dialogRef = this.dialog.open(CalendarDialogComponent, {
    //   disableClose: false,
    //   data: {
    //     option: 'eventDrag',
    //     calendar: events.event
    //   }
    // });
    //
    // dialogRef.afterClosed().subscribe(res => {
    //   console.log(events)
    //   console.log(res)
    //   if (res.isConfirmed) {
    //     events.event.setProp('title', res.eventFormdata.title)
    //     events.event.setProp('color', res.eventFormdata.color)
    //   }else{
    //     events.revert();
    //   }
    // })
  }
  handleEventDropInternal(events) {
    // function(events) {
      alert(events.event.title + " was dropped on " + events.event.start.toISOString());

      if (!confirm("Are you sure about this change?")) {
        events.revert();
      }
    // }
  }

}
